package dog;

public class Dog {

    // Fields:
    private String name;
    private int cutenessLevel;

    // Constructor
    public Dog(String name, int cuteness) {
        this.name = name;
        this.cutenessLevel = cuteness;
    }

    public String getName() {
        if (!(name == null && name.isEmpty()))
            return name;

        return "Make sure the dog's name is valid";
    }

    public int getCutenessLevel() {
        return cutenessLevel;
    }
}
