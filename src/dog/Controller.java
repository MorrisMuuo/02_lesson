package dog;

public class Controller {

    public static void start() {
        fillDogShelter();
    }

    private static void fillDogShelter() {

        DogShelter dogShelter = new DogShelter();

        // Create a new Dog object
        Dog lili = new Dog("Lili", 10);
        // add created dog to the dog shelter
        dogShelter.addDog(lili);

        Dog bella = new Dog("Bella", 12);
        dogShelter.addDog(bella);

        //searchDog method return an object Dog, so we can assign it to a variable
        // to get some info about the dog we found, for example its cuteness level
        Dog searchedDog = dogShelter.searchDog("Bella");
        System.out.println("The cuteness level of " + searchedDog.getName() + " is " + searchedDog.getCutenessLevel());
    }
}
