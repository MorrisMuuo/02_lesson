package dog;

public class DogShelter {

    private static int MAX_NUM_DOGS = 10;

    // Fields:
    private Dog[] dogs;
    private int dogsCount;

    // Constructor
    public DogShelter() {
        dogs = new Dog[MAX_NUM_DOGS];
        dogsCount = 0;

    }

    // Methods:
    void addDog(Dog dogs) {
        try {
            this.dogs[dogsCount] = dogs;
            dogsCount++;
        }catch (IndexOutOfBoundsException e){
            System.out.println("Shelter is already full of happy dogs:)");
        }

    }

   public Dog searchDog(String searchName) {
        for (int i = 0; i < dogsCount; i++) {
            if (dogs[i].getName().equals(searchName)) {
                return dogs[i];
            }
        }

        return null;
    }
}